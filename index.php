<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- <embed name="lostmojo" src="/song/song1.mp3" width="0" height="0" loop="true" autostart="true"> -->
    
    <meta charset="utf-8" />
    <meta name="theme-color" content="#BC8887" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Undangan Pernikahan Yeny & Leo</title>

    <meta http-equiv='cache-control' content='no-cache'>
    <meta http-equiv='expires' content='0'>
    <meta http-equiv='pragma' content='no-cache'>
    <meta property="og:title" content="Undangan Pernikahan Yeny & Leo - Rabu, 2 Februari 2022">
    <meta property="og:description" content="Undangan Pernikahan Yeny & Leo - Rabu, 2 Februari 2022 di Banaran 9 Resort, Hotel and SPA, Bawen, Kabupaten Semarang">
    <meta property="og:locale" content="id_ID" />

    <meta name="twitter:card" content="summary_large_image">
    <!-- Bulma Version 0.8.x-->
    <link rel="stylesheet" href="https://unpkg.com/bulma@0.8.0/css/bulma.min.css" />
    <link rel="stylesheet" href="https://unpkg.com/bulma-tooltip@0.1.5/dist/bulma-tooltip.min.css" />
    <link rel="stylesheet" type="text/css" href="css/menikah.css" />
    <link rel="stylesheet" type="text/css" href="css/jquery.countdown.css" />
    <script
      src="https://kit.fontawesome.com/2828f7885a.js"
      integrity="sha384-WAsFbnLEQcpCk8lM1UTWesAf5rGTCvb2Y+8LvyjAAcxK1c3s5c0L+SYOgxvc6PWG"
      crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="js/jquery.coundown.js"></script>
    <link rel="icon" type="image/png" href="image/favicon.png" />
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-23064379-20"></script>
    <!-- Begin Script for Countdown -->
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-23064379-20');
    </script>
    <!-- Begin Script for Countdown -->

    <!-- Github Button -->
    <script async defer src="https://buttons.github.io/buttons.js"></script>
  </head>

  <body>
    
    <!-- Begin Preloader -->
    <div class="preloader-wrapper">
      <div class="preloader">
        <img src="image/favicon.png" alt="Flower" />
      </div>
    </div>
    <!-- End Preloader-->
    <!-- Begin Header -->
    <div class="header-wrapper" id="home">
      <!-- Begin Hero -->
      <section class="hero is-large">
        <!-- Begin Mobile Nav -->
        <nav class="navbar is-transparent is-hidden-desktop">
          <!-- Begin Burger Menu -->
          <div class="navbar-brand is-fixed-top">
            <div class="navbar-burger burger" data-target="mobile-nav">
              <span></span>
              <span></span>
              <span></span>
            </div>
          </div>
          <!-- End Burger Menu -->
          <div id="mobile-nav" class="navbar-menu">
            <div class="navbar-end">
              <div class="navbar-item">
                <a class="navbar-item" href="#Waktu">
                  Waktu
                </a>
              </div>
              <div class="navbar-item">
                <a class="navbar-item" href="#lokasi">
                  Lokasi
                </a>
              </div>
              
              <div class="navbar-item">
                <a class="navbar-item" href="#tentang-yeny-leo">
                  Tentang Yeny dan Leo
                </a>
              </div>
              <div class="navbar-item">
                <a class="navbar-item" href="#gifts-and-greetings">
                  Gifts and Greetings
                </a>
              </div>
              <div class="navbar-item">
                <a class="navbar-item" href="#rsvp">
                  RSVP
                </a>
              </div>
            </div>
          </div>
        </nav>
        <!-- End Mobile Nav -->
        <!-- Begin Hero Content-->
        <div class="hero-body">
          <div class="container has-text-centered">
            <h1 class="subtitle">Undangan Pernikahan</h1>
            <h2 class="title">Yeny & Leo</h2>
            <h4 class="subtitle tempatwaktu">
              
                Rabu, 2 Februari 2022 
                </br>
                Banaran 9 Resort, Hotel and SPA, Bawen
                </br>
                Kabupaten Semarang
              
            </h4>
          </div>
          <!-- Start Countdown -->       
          <div>
            <ul id="hitungmundur" >
              <li><span class="days">00</span><p class="days_text">Hari</p></li>
              <li class="separator">:</li>
              <li><span class="hours">00</span><p class="hours_text">Jam</p></li>
              <li class="separator">:</li>
              <li><span class="minutes">00</span><p class="minutes_text">Menit</p></li>
              <li class="separator">:</li>
              <li><span class="seconds">00</span><p class="seconds_text">Detik</p></li>
            </ul>
            <div class="spasi">
            </div>
            <script type="text/javascript">
              var now = new Date();
              var day = now.getDate();
              var month = now.getMonth() + 1;
              var year = now.getFullYear() + 1;
          
              var nextyear = month + '/' + day + '/' + year + ' 07:07:07';
              var harih = '03/02/2022 12:00:00';

              $('#hitungmundur').countdown({
                date: harih, // TODO Date format: 07/27/2017 17:00:00
                offset: +7, // TODO Your Timezone Offset
                day: 'Hari',
                days: 'Hari'
              }, function () {
                alert('Done!');
              });
            </script>  
          </div>
          <!-- End Countdown -->
        </div>
        <!-- End Hero Content-->
        <!-- Begin Hero Menu -->
        <div class="hero-foot ">
          <div class="hero-foot--wrapper">
            <div class="columns">
              <div class="column is-12 hero-menu-desktop has-text-centered">
                <ul>
                  <li class="is-active">
                    <a href="#home">Home</a>
                  </li>
                  <li>
                    <a href="#Waktu">Waktu</a>
                  </li>
                  <li>
                    <a href="#lokasi">Lokasi</a>
                  </li>
                  <li>
                    <a href="#tentang-yeny-leo">Tentang Yeny & Leo</a>
                  </li>
                  <li>
                    <a href="#gifts-and-greetings">Gifts and Greetings</a>
                  </li>
                  <li>
                    <a href="#rsvp">RSVP</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <!-- End Hero Menu -->
      </section>
      <!-- End Hero -->
    </div>
    <!-- End Header -->

    <!-- Begin Main Content -->
    <div class="main-content">
    <!-- Begin regular-section-->
     <section class="section-light no-padding-top regular-section has-text-centered has-vertically-aligned-content" id="regular-section">
        
        <div data-aos="fade-up" data-aos-easing="linear">
          <img src="image/bismillah.png" class="bismillah has-text-centered" alt="Bismillahirrahmaanirrahiim">
        </div>

        <p class="bodytext" data-aos="fade-up" data-aos-easing="linear">
          Assalamu'alaikum Warahmatullahi Wabarakatuh.
          <br />
          Dengan memohon rahmat dan ridha Allah SWT, 
          <br />
          kami bermaksud mengundang Bapak/Ibu/Saudara/Saudari pada acara pernikahan kami
        </p>

        <span class="space40px"></span>
        <span class="space40px"></span>

        <div data-aos="fade-up" data-aos-easing="linear">
          <div class="nama-lengkap" >
            Yeny Rachmawati
          </div>
          <div class="ampersand">&</div>
          <div class="nama-lengkap">
          Muhammad Ign. Agus Leonardo
          </div>
           
        </div>
        <span class="space40px"></span>
        <div data-aos="fade-up" data-aos-easing="linear">
          <img src="image/divider-leaves.png" class="divider has-text-centered" alt="~~~">
        </div>
        <span class="space40px"></span>
        <span class="space40px"></span>
      </section>
      <!-- End regular-section-->    
      
      <!-- Begin Waktu Section -->
      <section class="section-dark" id="Waktu">
        <div class="container">

          <div class="column is-12 regular-section" data-aos="fade-up" data-aos-easing="linear">
            <h1 class="title has-text-centered section-title">Waktu</h1>
          </div>
          <div class="columns is-multiline" data-aos="fade-up" data-aos-easing="linear">
            <div
              class="column is-4 has-vertically-aligned-content">
            
              <p class="is-larger has-text-centered">
                  <div class="waktu tanggal-hari has-text-centered">Rabu</div> 
                  <div class="waktu tanggal-angka has-text-centered">2</div>
                  <div class="waktu tanggal-bulan has-text-centered">Februari 2022</div>
              </p>
            
            </div>
            <div class="column is-4 has-vertically-aligned-content">
            
              <p class="waktu is-larger has-text-centered">
                Akad Nikah:
                <br>
                <strong>8.00 WIB</strong>
              </p>
            
            </div>

            <div class="column is-4 has-vertically-aligned-content">
              
              <h1 class="waktu is-larger has-text-centered">
                Resepsi:
                <br>
                <strong>11.00  - 13.30 WIB</strong>
              </h1>
              
              
            </div>

          </div>
        </div>
        <div class="space40px"></div>
        <div class="main-content has-text-centered" data-aos="fade-up" data-aos-easing="linear">
          <a href="https://calendar.google.com/event?action=TEMPLATE&tmeid=MWYyYWFidTlqMzY1NnY4cWZmZmtsYWpjcGZfMjAyMjAyMDJUMDUwMDAwWiB5ZW55cmFjaG1hd2F0aTI5QG0&tmsrc=yenyrachmawati29%40gmail.com" target="_blank" class="button has-tooltip btn-cta" data-tooltip="Add to Calendar" target="_blank" data-aos="zoom-in">
            <i class="far fa-calendar-plus"></i>
            &nbsp;&nbsp;Google Calendar
          </a>
        </div>
      </section>
      <!-- End Waktu Content -->

      <!-- Begin Lokasi Section -->
      <section class="section-darker" id="lokasi" >
        <div class="container">
          <div class="column is-12 regular-section" data-aos="fade-up" data-aos-easing="linear">
            <h1 class="title has-text-centered section-title">Lokasi</h1>
            <p class="tempat is-larger has-text-centered">
              <strong>Banaran 9 Resort, Hotel and SPA</strong>
              <br>
              Jl. Raya Bawen - Solo KM. 1,5 Bawen Baan, Baan, Asinan
              <br>
               Bawen, Semarang, Jawa Tengah 50661
              <br>
            </p>
          </div>
          <div class="section-map" data-aos="fade-up" data-aos-easing="linear">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3957.86400871441!2d110.43923951437074!3d-7.256314273300049!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7083cda20e17fb%3A0x2148445a882b2be6!2sBanaran%209%20Resort%20Hotel%20%26%20SPA!5e0!3m2!1sen!2sid!4v1632548262802!5m2!1sen!2sid" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
            
          </div>
          <div class="space40px"></div>
          <div class="has-text-centered" data-aos="fade-up" data-aos-easing="linear">
            <a href="https://www.google.com/maps/place/Banaran+9+Resort+Hotel+%26+SPA/@-7.25632,110.441428,16z/data=!4m8!3m7!1s0x0:0x2148445a882b2be6!5m2!4m1!1i2!8m2!3d-7.2563426!4d110.4414266?hl=en" class="button btn-cta" target="_blank" data-aos="zoom-in">
              <i class="far fa-directions"></i>
              &nbsp;&nbsp;Google Maps
            </a>
          </div>
        </div>
      </section>
      <!-- End Lokasi Content -->

      <!-- Begin Tentang Yeny and Leo -->
      <section class="section-light regular-section is-fullheight" id="tentang-yeny-leo">
        <div class="container">
          <div class="columns is-multiline">  
          <div class="column has-text-centered is-12 prolog">
            <h1 class="title has-text-centered section-title" data-aos="fade-up" data-aos-easing="linear">Tentang Yeny dan Leo</h1>
            

          
          <!-- IMAGES -->  
        <div class=" tile">
          <div class="tile is-ancestor">
          <div class="tile is-parent">
            <article class="tile is-child notification foto2" data-aos="fade-up" data-aos-easing="linear">
              <div class="content">
                <p class="title-foto">
                  September 2015 - Juli 2019
                </p>
                <p class="subtitle-foto">
                  Satu kelompok AMT, satu kampus, dan lulus.
                </p>   
              </div>            
            </article>
          </div>
          <div class="tile is-parent">
            <article class="tile is-child notification foto2" data-aos="fade-up" data-aos-easing="linear">
              <div class="content">  
                <p class="title-foto">
                  Juni 2020
                </p>
                <p class="subtitle-foto">
                  Bertemu kembali, Cerita Cinta dimulai
                </p>
              </div>
            </article>
          </div>
        </div>
      </div>
      <div class="space24px"></div>
      <div class="tile is-ancestor">
        <div class="tile is-parent">
          <article class="tile is-child notification foto2" data-aos="fade-up" data-aos-easing="linear">
            <div class="content">
              <p class="title-foto">Mei 2020</p>
              <p class="subtitle-foto">Pertemuan 2 keluarga</p>
            </div>
          </article>
        </div>
        <div class="tile is-parent">
          <article class="tile is-child notification foto2" data-aos="fade-up" data-aos-easing="linear">
            <div class="content">
              <p class="title-foto">Mei 2020</p>
              <p class="subtitle-foto">Lamaran</p>
            </div>
          </article>
        </div>
        <div class="tile is-parent">              
          <article class="tile is-child notification foto2" data-aos="fade-up" data-aos-easing="linear">
            <div class="content">
              <p class="title-foto">Februari 2022</p>
              <p class="subtitle-foto">Menikah</p>
            </div>
          </article>
        </div>
      </div>   
          <!-- IMAGES -->
          <div class="space40px"></div>
            <div data-aos="fade-up" data-aos-easing="linear">
              <img src="image/divider-leaves.png" class="divider has-vertically-align" alt="~~~">
            </div> 
            <div class="space40px"></div>
            <div class="space40px"></div>

          </div>
        </div>
      </section>
      <!-- End Tentang Yeny dan Leo -->
       <section class="section-light contact" id="gifts-and-greetings">
        <div class="container">
          <div class="columns is-multiline">          
            <div class="column is-12 prolog">
              <h1 class="title has-text-centered section-title" data-aos="fade-up" data-aos-easing="linear">Gifts and Greetings</h1>
            </div>
            <div class="column is-12 prolog has-text-centered" data-aos="fade-up" data-aos-easing="linear">
            
              <!-- <p class="h2 subtitle">
                Merupakan suatu kehormatan dan kebahagiaan bagi kami
                <br>
                apabila Bapak/Ibu/Saudara/Saudari berkenan hadir
                memberikan doa restu.
                <br>
                <br>
                Jika bisa hadir kami tunggu konfirmasinya, 
                <br>
                Informasi: Di meja penerima tamu akan kami sediakan hand sanitizer dan pemeriksaan suhu tubuh.
                <br>
                <br>
                Jika tidak memungkinkan untuk hadir di pernikahan kami,tidak mengapa,
                <br> 
                semoga bisa berjumpa di lain kesempatan
                <br>
                <br>
                Stay safe & jaga kesehatan ya :)
                </p>
     
              <a href="https://api.whatsapp.com/send?phone=628982424868&text=Halo%20Yeny,%20saya%20akan%20datang%20di%20acara%20pernikahan&source=&data=" class="button btn-whatsapp" target="_blank" data-aos="zoom-in">
                <i class="fab fa-whatsapp"></i>
                &nbsp;&nbsp;Kabari Yeny
              </a>
              

          
              <a href="https://api.whatsapp.com/send?phone=6282114575296&text=Halo%20Leo,%20saya%20akan%20datang%20di%20acara%20pernikahan&source=&data=" class="button btn-whatsapp" target="_blank" data-aos="zoom-in">
                <i class="fab fa-whatsapp"></i>
                &nbsp;&nbsp; Kabari Leo
              </a >
              <div class="space40px"></div>

              <div class="space40px"></div>
              <div data-aos="fade-up" data-aos-easing="linear">
                <img src="image/divider-leaves.png" class="divider has-text-centered" alt="~~~">
              </div>
              <div class="space40px"></div>
              <p class="h2 subtitle" data-aos="fade-up" data-aos-easing="linear">
                Kami yang berbahagia,
                <br>
                <a class="instagram":" href="https://instagram.com/yenyrachmawati" target="_blank" alt="Instagram Yeny">
                  <i class="fab fa-instagram"></i> @yenyrachmawati
                </a>
                &nbsp
                <a class="instagram" href="https://instagram.com/igaraleon_" target="_blank" alt="Instagram Leo">
                  <i class="fab fa-instagram"></i> @igaraleon_
                </a>
              </p> -->

            </div>
 
         </div>
        </div>
      </section>
      <!-- Begin RSVP Content -->
      <section class="section-dark contact" id="rsvp">
        <div class="container">
          <div class="columns is-multiline">          
            <div class="column is-12 prolog">
              <h1 class="title has-text-centered section-title" data-aos="fade-up" data-aos-easing="linear">Konfirmasi Kehadiran</h1>
            </div>
            <div class="column is-12 prolog has-text-centered" data-aos="fade-up" data-aos-easing="linear">
              <p class="h2 subtitle">
                Merupakan suatu kehormatan dan kebahagiaan bagi kami
                <br>
                apabila Bapak/Ibu/Saudara/Saudari berkenan hadir
                memberikan doa restu.
                <br>
                <br>
                Jika bisa hadir kami tunggu konfirmasinya, 
                <br>
                Informasi: Di meja penerima tamu akan kami sediakan hand sanitizer dan pemeriksaan suhu tubuh.
                <br>
                <br>
                Jika tidak memungkinkan untuk hadir di pernikahan kami,tidak mengapa,
                <br> 
                semoga bisa berjumpa di lain kesempatan
                <br>
                <br>
                Stay safe & jaga kesehatan ya :)
                </p>
     
              <a href="https://api.whatsapp.com/send?phone=628982424868&text=Halo%20Yeny,%20saya%20akan%20datang%20di%20acara%20pernikahan&source=&data=" class="button btn-whatsapp" target="_blank" data-aos="zoom-in">
                <i class="fab fa-whatsapp"></i>
                &nbsp;&nbsp;Kabari Yeny
              </a>
              

          
              <a href="https://api.whatsapp.com/send?phone=6282114575296&text=Halo%20Leo,%20saya%20akan%20datang%20di%20acara%20pernikahan&source=&data=" class="button btn-whatsapp" target="_blank" data-aos="zoom-in">
                <i class="fab fa-whatsapp"></i>
                &nbsp;&nbsp; Kabari Leo
              </a >
              <div class="space40px"></div>

              <div class="space40px"></div>
              <div data-aos="fade-up" data-aos-easing="linear">
                <img src="image/divider-leaves.png" class="divider has-text-centered" alt="~~~">
              </div>
              <div class="space40px"></div>
              <p class="h2 subtitle" data-aos="fade-up" data-aos-easing="linear">
                Kami yang berbahagia,
                <br>
                <a class="instagram" href="https://instagram.com/yenyrachmawati" target="_blank" alt="Instagram Yeny">
                  <i class="fab fa-instagram"></i> @yenyrachmawati
                </a>
                &nbsp
                <a class="instagram" href="https://instagram.com/igaraleon_" target="_blank" alt="Instagram Leo">
                  <i class="fab fa-instagram"></i> @igaraleon_
                </a>
              </p>

            </div>
 
         </div>
        </div>
      </section>
     
      <!-- End RSVP Content -->
    </div>
    <!-- End Main Content -->

    <!-- Begin Footer -->
    <div class="footer">
      <div class="container">
        <a href="#" class="has-vertically-align">
          <p class="has-vertically-align">2022 &copy M I A Leonardo</p>
        </a>
        <a href="https://bulma.io" class="has-vertically-align">
          <img src="https://bulma.io/images/made-with-bulma.png" alt="Made with Bulma">
        </a>
      </div>
    </div>
    <!-- End Footer -->

    <!-- Scripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="js/menikah.js"></script>
    <link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">
    <script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>
    <script>
      AOS.init({
        easing: "ease-out",
        duration: 800,
      });
    </script>
    <script>
      $(document).ready(function() {
        var audio = new Audio('https://doc-00-18-docs.googleusercontent.com/docs/securesc/istd55afbki13o8ijh3in8rkdq8o2f7t/b7acq4251kimvh5t2sgqlkpf21hc6hgt/1633235175000/04199246133259550861/04199246133259550861/1C1oz-ceKsPfE0jY-ps7bI43bYNhMfUgA?e=download&authuser=1');
        //https://doc-08-18-docs.googleusercontent.com/docs/securesc/istd55afbki13o8ijh3in8rkdq8o2f7t/5bscbmg5obd3r2cbepelnl0c8trndreg/1633235250000/04199246133259550861/04199246133259550861/1HOxFv7J44UOPQS9JA3Cc_B-P-YoBTJ3w?e=download&authuser=1

        //https://doc-10-18-docs.googleusercontent.com/docs/securesc/istd55afbki13o8ijh3in8rkdq8o2f7t/4orp70d5787p66vgpksp4ff0ed92gf2f/1633235250000/04199246133259550861/04199246133259550861/1BKtC0sN5IXonhkcwEhBpQH3FrBC3ofgs?e=download&authuser=1

        //https://doc-10-18-docs.googleusercontent.com/docs/securesc/istd55afbki13o8ijh3in8rkdq8o2f7t/55ved534oscr9p7om6r15ehna83163gr/1633235250000/04199246133259550861/04199246133259550861/1dExVdmBdg3ILkNVzxUrCSCwuwUntvtCA?e=download&authuser=1

        if (typeof audio.loop == 'boolean')
        {
            audio.loop = true;
        }
        else
        {
            audio.addEventListener('ended', function() {
                this.currentTime = 0;
                this.play();
            }, false);
        }
        audio.play();
      });
    </script>
  </body>
</html>
